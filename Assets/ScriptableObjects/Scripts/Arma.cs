using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Weapon", menuName = "ScriptableObjects/Arma")]
public class Arma : ScriptableObject
{
    public string WeaponName;
    public int WeaponID;
    public Sprite sprite;
    
    //private SpriteRenderer sr;

    public float totalBullets; //Balas en total
    public float startingTotalBullets;
    public float bullets; // Balas en este cargador
    public float startingCurrentBullets;
    [SerializeField] float addedBullets; //Balas a�adidas al coger un cargador
    
    public float timeBetweenShoots;
    public float reloadTime;
    public GameObject bulletType;
    public float bulletForce;

    private Camera camera;
    
    private Transform firePoint;
    //private GameObject player;

    public AudioClip shootSound;

    private void OnEnable()
    {
        bullets = startingCurrentBullets;
        totalBullets = startingTotalBullets - startingCurrentBullets;

        Debug.Log("ARMA");

       //Los if no deberian hacer falta, pero si no da errores sin estar ejecutando �? 
       if(GameObject.Find("equipedGun")) firePoint = GameObject.Find("equipedGun").transform;
       if (GameObject.Find("Main Camera")) camera = GameObject.Find("Main Camera").GetComponent<Camera>();
       //if (GameObject.Find("Player")) player = GameObject.Find("Player");
    }


    public void GunRotation()
    {
        RefillParameters(); //No es la manera mas bonita, pero es una manera

        var mouse = camera.ScreenToWorldPoint(Input.mousePosition);
        mouse.z = 0;

        Vector3 lookAt = mouse - firePoint.position;
        firePoint.right = lookAt;    
    }

    void RefillParameters()
    {
        //Al cambiar de escena se pierde la referencia, por ello reviso cada vez
        if (camera == null) camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        if (firePoint == null) firePoint = GameObject.Find("equipedGun").transform;
    }

    public void AddBullets()
    {
        totalBullets += addedBullets;
    }

    public void Shoot()
    {
        if (bullets >= 1)
        {
            GameObject bala = Instantiate(bulletType, firePoint.position, firePoint.rotation);
            bala.GetComponent<Rigidbody2D>().velocity = firePoint.right * bulletForce;

            bullets--;
        }
    }

    //Dentro del scriptable object no se puede hacer la corrutina, aqui hago normal y en el player controller le pongo que se espere
    public void Reload()
    { 
        if (bullets == 0)
        {
            if (startingCurrentBullets <= totalBullets)
            {
                //No le quedan balas en el cargador, y tiene las suficientes, lo llena entero
                bullets += startingCurrentBullets;
                totalBullets -= startingCurrentBullets;
            }
            else
            {
                //No le quedan balas en el cargador, ni en las totales, llena solo lo que falte
                bullets += totalBullets;
                totalBullets -= totalBullets;
            }
        }
        else
        {
            //Si le queda alguna en el cargador, hay que hacer la resta para saber cuantas a�adir sin que se pase del limite
            var balasRestantes = startingCurrentBullets - bullets;

            //Debug.Log("Le tengo que sumar " + balasRestantes + " balas");

            if (balasRestantes <= totalBullets)
            {
                bullets += balasRestantes;
                totalBullets -= balasRestantes;
            }
            else
            {
                //No le quedan balas en el cargador, ni en las totales, llena solo lo que falte
                bullets += totalBullets;
                totalBullets -= totalBullets;
            }
        }
    }

    public void RestartValues()
    {
        totalBullets = startingTotalBullets;
        bullets = startingCurrentBullets;
    }
    void Update()
    {
        Debug.Log("hola");
    }
}
