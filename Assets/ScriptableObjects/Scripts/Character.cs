using UnityEngine;

public class Character : ScriptableObject
{
    public float health;
    public float maxHealth;
    public float speed;
}
