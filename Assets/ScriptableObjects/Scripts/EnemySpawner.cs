using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public float timeBetweenEnemies; //Tiempo entre los enemigos
    [SerializeField] private float spawnCooldown; //Contador del cooldown

    public int enemiesSpawnAtOnce; //Cuantos enemigos spawnean al mismo tiempo
    public GameObject[] enemiesList; //Lista de los enemigos que pueden spawnear
    
    public int oleadas; //Cuantas oleadas de enemigos pueden aparecer
    [SerializeField] private int oleada; //En que oleada se encuentra actualmente

    public int totalEnemies; //Todos los enemigos que pueden aparecer

    public Vector2 enemiesX;
    public Vector2 enemiesY;

    public delegate void Spawner();
    public static event Spawner OnAllEnemiesDied;

    [SerializeField] int minEnemies,maxEnemies; //El numero minimo y maximo de enemigos por oleada
    [SerializeField] int minOleadas, maxOleadas; // El numero minimo y maximo de oleadas

    private void Start()
    {
        spawnCooldown = timeBetweenEnemies  / 2;
        oleada = 0;

        enemiesSpawnAtOnce = Random.Range(minEnemies, maxEnemies + 1); //Los enemigos que aparecen cada oleada son aleatorios.
        oleadas = Random.Range(minOleadas, maxOleadas + 1); //El numero de oleadas que habr�n

        //El numero de enemigos que saldr�n ser� los enemigos de cada oleada multiplicado por el numero de oleadas
        totalEnemies = enemiesSpawnAtOnce * oleadas;  
    }

    private void Update()
    {        
        if (spawnCooldown <= 0) Spawn();
        else spawnCooldown -= Time.deltaTime;
    }

    void Spawn()
    {
        if (oleada < oleadas)
        {
            for (int i = 0; i < enemiesSpawnAtOnce; i++)
            {
                Vector2 spawnPosition = new Vector2(Random.Range(enemiesX.x, enemiesX.y),
                    Random.Range(enemiesY.x, enemiesY.y));

                var randomEnemyIndex = Random.Range(0, enemiesList.Length);

                Instantiate(enemiesList[randomEnemyIndex], spawnPosition, Quaternion.identity);
            }

            oleada++;
            spawnCooldown = timeBetweenEnemies;
        }
    }

    public void DecreaseEnemy()
    {
        totalEnemies--;
        if (totalEnemies == 0) OnAllEnemiesDied();
    }
}
