using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelsList", menuName = "ScriptableObjects/LevelLists")]
public class LevelsList : ScriptableObject
{
    public List<SceneAsset> ListaDeEscenas;
}
