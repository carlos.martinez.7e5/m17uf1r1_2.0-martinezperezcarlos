using UnityEngine;

[CreateAssetMenu(fileName = "New Character", menuName = "ScriptableObjects/Player")]
public class Player : Character
{
    public float startingSpeed;

    public Arma ArmaEquipada;
    public Arma[] ArmasTotales; //Todas las armas que puede tener
    
    public Arma[] ArmasQueTengo; //Las armas que tiene ahora
    public Arma[] ArmasInicio; //Las armas con las que comienza

    public float maxEscudo; //El valor m�ximo que puede tener el escudo 
    public float Escudo; //Valor de escudo que puede comprar en las tiendas
    public int Dinero; //Dinero que tiene para comprar en las tiendas, se 

    private void OnEnable()
    {
        //Para que cada vez que comience la partida tenga la vida al maximo
        health = maxHealth;

        maxEscudo = maxHealth / 2; //Lo m�ximo de escudo que puede tener es la mitad de la vida
        Escudo = 0;

        ArmasQueTengo = ArmasInicio;
        ArmaEquipada = ArmasQueTengo[0];
    }

    public void PickUpWeapon(Arma toAddWeapon)
    {
        // Arma w = new Arma();

        Arma w = (Arma)ScriptableObject.CreateInstance(typeof(Arma));

        // GenerateItem o = (GenerateItem)ScriptableObject.CreateInstance(typeof(GenerateItem));

        foreach (var arma in ArmasTotales) if (arma.WeaponID == toAddWeapon.WeaponID) w = arma;

        var auxArray = new Arma[ArmasQueTengo.Length + 1];

        for (int i = 0; i < auxArray.Length - 1; i++)
        {
            auxArray[i] = ArmasQueTengo[i];
        }

        auxArray[auxArray.Length - 1] = w;

        ArmasQueTengo = auxArray;
        ArmaEquipada = w;
    }

    public void RestartValues()
    {
        health = maxHealth;
    }
}
