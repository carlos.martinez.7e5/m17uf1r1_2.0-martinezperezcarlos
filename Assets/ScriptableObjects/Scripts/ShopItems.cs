using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ShopItemsList", menuName = "ScriptableObjects/ShopItemList")]
public class ShopItems : ScriptableObject
{
    public List<Item> itemList;
}
