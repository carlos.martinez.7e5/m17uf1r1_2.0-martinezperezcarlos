using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    [SerializeField] float _lifeTime;
    public float _attack;
    private IDamageable player;

    private void Start()
    {
        player = GameObject.Find("Player").GetComponent<IDamageable>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_lifeTime <= 0) Destroy(this.gameObject);
        else _lifeTime -= Time.deltaTime;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        IDamageable i = collision.collider.GetComponent<IDamageable>();
        EnemyBulletBehaviour b = collision.collider.GetComponent<EnemyBulletBehaviour>();

        //Para hacer da�o a los enemigos / IDamageable
        if (i != null)
        {
            Destroy(this.gameObject);

            //Para que no se puede herir a si mismo
            if(i != player)
            {
                //Destroy(this.gameObject);
                i.TakeDamage(_attack);
            }
        }

        //Para eliminar ambas balas si choca contra las enemigas
        if (b != null)
        {
            Destroy(this.gameObject);
            Destroy(collision.gameObject);
        }

        //Si no es ni lo uno ni lo otro simplemente se destruye este y ya est�
        if (i == null && b == null) Destroy(this.gameObject);
    }
}
