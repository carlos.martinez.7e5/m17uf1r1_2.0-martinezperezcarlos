using UnityEngine;

public class DefaultShoot : MonoBehaviour
{
    [SerializeField] private Arma wData;
    private Transform firePoint;

    private void Start() { firePoint = GameObject.Find("equipedGun").transform; }

    public void Shoot()
    {
        var bulletType = wData.bulletType;
        var bulletForce = wData.bulletForce;

        GameObject bala = Instantiate(bulletType, firePoint.position, Quaternion.identity);
        bala.GetComponent<Rigidbody2D>().velocity = firePoint.right * bulletForce;
    }
}
