using UnityEngine;

public class ElectricBullet : BulletBehaviour
{
    [SerializeField] float secondsStunned;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(!collision.isTrigger)
        {
            IStuneable i = collision.GetComponent<IStuneable>();
            if (i != null) i.GetStunned(secondsStunned);
        }
    }
}
