using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletBehaviour : MonoBehaviour
{
    [SerializeField] float _lifeTime;
    [SerializeField] float _attack;

    // Update is called once per frame
    void Update()
    {
        if (_lifeTime <= 0) Destroy(this.gameObject);
        else _lifeTime -= Time.deltaTime;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        IDamageable i = collision.collider.GetComponent<IDamageable>();

        if (i != null)
        {
            Destroy(this.gameObject);
            i.TakeDamage(_attack);
        }
    }
}
