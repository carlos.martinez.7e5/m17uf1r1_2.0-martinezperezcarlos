using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ShootController : MonoBehaviour
{
    [SerializeField] protected Arma weaponData;
    protected Transform firePoint;

    private void Start() { firePoint = GameObject.Find("equipedGun").transform; }
    
    public abstract void Shoot();
}
