using UnityEngine;

public class BrekeableBox : MonoBehaviour, IDropeable, IDamageable
{
    [SerializeField] float _dropChance;
    [SerializeField] GameObject[] _dropeableItems;

    float _dropGeneratedChance;

    [SerializeField] float _vida;

    public void DropItem()
    {
        _dropGeneratedChance = Random.Range(1, 100);
        
        if (_dropGeneratedChance <= _dropChance) {

            Debug.Log("Voy a elegir que item voy a spawnear");
            var itemDroped = Random.Range(0, _dropeableItems.Length);
            Debug.Log("El " + itemDroped);

            Instantiate(_dropeableItems[itemDroped], transform.position, Quaternion.identity); 
        }
        
        Destroy(this.gameObject);
    }

    public void TakeDamage(float damageTaken)
    {
        _vida -= damageTaken;
        Debug.Log("Ay! Soy " + this.gameObject.name + " y me han hecho " + damageTaken + " puntos de da�o. Ahora me quedan " + _vida);

        if (_vida <= 0)
        {
            Debug.Log("Paso por este if siendo mi vida " + _vida);
            DropItem();
        }
    }
}
