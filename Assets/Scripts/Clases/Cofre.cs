
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cofre : MonoBehaviour, IDropeable
{
    [SerializeField] float _dropChance;
    [SerializeField] GameObject[] _dropeableItems;

    float _dropGeneratedChance;

    [SerializeField] bool _canOpen;

    private void OnTriggerEnter2D(Collider2D collision) { _canOpen = true; }
    

    private void OnTriggerExit2D(Collider2D collision) { _canOpen = false; }
    
    void OpenChest() { if (_canOpen) DropItem(); }
     
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E)) { OpenChest(); }
    }

    public void DropItem()
    {
        _dropGeneratedChance = Random.Range(1, 100);
        var _itemDropped = Random.Range(1, _dropeableItems.Length - 1);
        Debug.Log("Ha salido el " + _itemDropped);
        
        if (_dropGeneratedChance <= _dropChance) { Instantiate(_dropeableItems[_itemDropped], transform.position, Quaternion.identity); }
        Destroy(this.gameObject);
    }
}
