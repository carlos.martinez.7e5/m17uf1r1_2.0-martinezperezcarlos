using System.Collections.Generic;
using UnityEngine;

public class BaseEnemy : MonoBehaviour
{
    protected SpriteRenderer _sr;
    
    [SerializeField] protected Enemy enemyData;
    protected Enemy instance;

    [SerializeField] protected AudioClip dieClip;

    public delegate void BEnemy();
    public static event  BEnemy onEnemyKilled;

    [SerializeField] protected int DropChance;
    [SerializeField] protected Item itemDrop;

    protected EnemySpawner spawner;

    

    protected void Die()
    {
        onEnemyKilled();
    }
}
