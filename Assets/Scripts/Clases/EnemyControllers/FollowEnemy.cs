using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowEnemy : BaseEnemy, IDamageable, IStuneable
{
    Color _originalColor;
    [SerializeField] private float explosionDamage;

    [SerializeField] AudioClip explodeClip;

    Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();

        _sr = GetComponent<SpriteRenderer>();
        _originalColor = _sr.color;

        //Crea una instancia para poder bajarle la vida sin afectar al scriptableObject original
        instance = (Enemy)ScriptableObject.CreateInstance(typeof(Enemy));

        //Al hacer el instance se crea en 0, �lo tengo que poner de nuevo?
        instance.health = enemyData.maxHealth;
        instance.speed = enemyData.speed;

        spawner = GameObject.Find("EnemySpawner").GetComponent<EnemySpawner>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player")) _sr.color = Color.red;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        Yua yua = collision.GetComponent<Yua>();

        if (yua != null)
        {
            //Hace falta cambiar el modo "sleep" para que cuente todo el tiempo que est� dentro del trigger
            yua.GetComponent<Rigidbody2D>().sleepMode = RigidbodySleepMode2D.NeverSleep;
            gameObject.transform.position = Vector2.MoveTowards(transform.position, yua.transform.position, enemyData.speed * Time.deltaTime);

            if (yua.transform.position.x > this.transform.position.x) _sr.flipX = true;
            else _sr.flipX = false;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Yua yua = collision.GetComponent<Yua>();

        //No se exactamente en que puede afectar, asi que al salir lo dejo como estaba
        if (yua != null) yua.GetComponent<Rigidbody2D>().sleepMode = RigidbodySleepMode2D.StartAwake;

        _sr.color = _originalColor;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        IDamageable i = collision.gameObject.GetComponent<IDamageable>();

        //Hace da�o al jugador
        if (i != null)
        {
            i.TakeDamage(explosionDamage);

            //Ya no llama a la funci�n de explotar, sino que a la animaci�n Y la animaci�n llama a la funci�n
            animator.SetTrigger("Explode");
            //Explode();
        }
    }

    private void Explode()
    {
        //GetComponent<AudioSource>().PlayOneShot(explodeClip);
        spawner.DecreaseEnemy();
        Destroy(this.gameObject);
    }

    private void ExplodeSound()
    {
        GetComponent<AudioSource>().PlayOneShot(explodeClip);
    }

    public void TakeDamage(float damageTaken)
    {
        instance.health -= damageTaken;
        GetComponent<AudioSource>().PlayOneShot(dieClip);

        if (instance.health <= 0)
        {
            animator.SetTrigger("Die");
            //StartCoroutine(Die());
        }
    }

    new IEnumerator Die()
    {
        base.Die();

        //Para que no se destruya antes de que haga el sonido
        GetComponent<AudioSource>().PlayOneShot(dieClip);
        yield return new WaitForSeconds(dieClip.length);
        spawner.DecreaseEnemy();
        Drop();
        Destroy(this.gameObject);
    }

    public void GetStunned(float timeStunned)
    {
        StartCoroutine(StunCorroutine(timeStunned));
    }

    IEnumerator StunCorroutine(float timeStunned)
    {
        SwitchColliders(false);
        yield return new WaitForSeconds(timeStunned);
        SwitchColliders(true);
    }
    public void SwitchColliders(bool enabled)
    {
        var colliders = this.gameObject.GetComponents<Collider2D>();
        foreach (var c in colliders)
        {
            if (c.isTrigger)
            {
                c.enabled = enabled;
                Debug.Log(c.enabled);
            }
        }
    }
    public void DisableCollisions()
    {
        //Para evitar que durante la animaci�n tambi�n pueda matar
        GetComponent<BoxCollider2D>().enabled = false;
    }

    void Drop()
    {
        var _dropGeneratedChance = Random.Range(1, 100);

        if (_dropGeneratedChance <= DropChance)
        { 
            Instantiate(itemDrop, transform.position, Quaternion.identity);
        }
    }
}
