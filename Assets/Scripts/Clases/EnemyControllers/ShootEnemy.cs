using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootEnemy : BaseEnemy, IDamageable, IStuneable
{
    [SerializeField] private Transform firePoint;

    [SerializeField] private float _timeBetweenShoot;
    private float _cooldown;
    [SerializeField] private float _bulletSpeed;
    [SerializeField] private GameObject _bullet;

    private GameObject target;

    private void Start()
    {
        _sr = GetComponent<SpriteRenderer>();

        _cooldown = _timeBetweenShoot;

        //Crea una instancia para poder bajarle la vida sin afectar al scriptableObject original
        instance = (Enemy)ScriptableObject.CreateInstance(typeof(Enemy));

        //Al hacer el instance se crea en 0, �lo tengo que poner de nuevo?
        instance.health = enemyData.maxHealth;
        instance.speed = enemyData.speed;

        if (GameObject.Find("Player") != null) target = GameObject.Find("Player");

        spawner = GameObject.Find("EnemySpawner").GetComponent<EnemySpawner>();
    }

    private void Update()
    {
        LookAtTarget();

        if (_cooldown <= 0) Shoot();
        else _cooldown -= Time.deltaTime;
    }

    void Shoot()
    {
        GameObject bala = Instantiate(_bullet, firePoint.transform.position, Quaternion.identity);
        bala.GetComponent<Rigidbody2D>().velocity = transform.right * _bulletSpeed;

        _cooldown = _timeBetweenShoot;
    }

    void LookAtTarget()
    {
        if (target != null)
        {
            //Para que sepa donde tiene que disparar
            Vector2 direction = target.transform.position - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

            //Para que se gire dependiendo de la posicion del jugador
            if (target.transform.position.x < transform.position.x) this.transform.localScale = new Vector3(1, -1, 1);
            else this.transform.localScale = new Vector3(1, 1, 1);
        }
    }

    public void TakeDamage(float damageTaken)
    {
        instance.health -= damageTaken;
        GetComponent<AudioSource>().PlayOneShot(dieClip);

        if (instance.health <= 0) StartCoroutine(Die());  
    }

    new IEnumerator Die()
    {
        Debug.Log("Comienzo la corrutina");
        base.Die(); //Evento para que sume al DataHolder

        //Para que no se destruya antes de que haga el sonido
        GetComponent<AudioSource>().PlayOneShot(dieClip);
        yield return new WaitForSeconds(dieClip.length);
        Drop();
        spawner.DecreaseEnemy();
        Destroy(this.gameObject);
    }

    public void GetStunned(float timeStunned)
    {
        StartCoroutine(StunCorroutine(timeStunned));
    }

    IEnumerator StunCorroutine(float timeStunned)
    {
        Debug.Log("Soy el esqueleto y me quedo stuned");
        SwitchActive(false);
        yield return new WaitForSeconds(timeStunned);
        SwitchActive(true);
    }

    public void SwitchActive(bool enabled)
    {
        var enemy = GetComponent<ShootEnemy>();
        enemy.enabled = enabled;

        //Puede hacer m�s cositas, de momento solo se queda parado 
    }

    void Drop()
    {
        var _dropGeneratedChance = Random.Range(1, 100);

        if (_dropGeneratedChance <= DropChance)
        {
            Instantiate(itemDrop, transform.position, Quaternion.identity);
        }
    }
}