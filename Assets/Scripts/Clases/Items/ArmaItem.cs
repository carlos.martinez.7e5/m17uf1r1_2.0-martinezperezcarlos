using UnityEngine;

public class ArmaItem : Item
{
    [SerializeField] Arma arma;
    [SerializeField] Player player;

    //En este caso le paso el parametro porque es obligatorio por herencia pero no hace falta
    public override void Interact(Yua yua)
    {
        for (int i = 0; i < player.ArmasQueTengo.Length; i++)
        {
            if (arma.WeaponID == player.ArmasQueTengo[i].WeaponID)
            {
                player.ArmasQueTengo[i].AddBullets();

                //Si cojo / compro un arma que ya tengo, le sumo balas y la cambio para dar feedback
                player.ArmaEquipada = player.ArmasQueTengo[i];
                
                return; //Para que salga de la funcion
            }
        }

        //Aqu� solo llega si no tiene el arma
        player.PickUpWeapon(arma);
    }
}
