using UnityEngine;

public class Escudo : Item
{
    [SerializeField] float shieldValue;

    public override void Interact(Yua yua)
    {
        yua.RestoreShield(shieldValue);
    }
}
