using UnityEngine;

public abstract class Item : MonoBehaviour
{
    //En teoria con las corrutinas sera mas simple
    [SerializeField] protected float _lifeTime;
    [SerializeField] float _timeBetweenParpadeo;
    float _despawnCooldown;
    float _parpadeoCooldown;
    
    bool visible; 
    Color _originalColor;

    public SpriteRenderer sr; //Necesario para la tienda
    public int price;

    public float rotationSpeed;
    Transform t;

    private void Start()
    {
        _despawnCooldown = _lifeTime;
        _parpadeoCooldown = _timeBetweenParpadeo;

        visible = true;
        _originalColor = GetComponent<SpriteRenderer>().color;

        t = this.transform;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision.gameObject.name);
        Yua yua = collision.GetComponent<Yua>();

        if (yua != null)
        {
            yua.GetComponent<AudioSource>().PlayOneShot(yua.pickUpItem);
            Interact(yua);
            Destroy(this.gameObject);
        }
    }

    //Funcion unica para cada item distinto.
    public abstract void Interact(Yua yua);

    private void Update()
    {
        Despawn();
        Spin();
    }

    void Despawn()
    {
        if (_despawnCooldown <= 0) Destroy(this.gameObject);
        else 
        {
            _despawnCooldown -= Time.deltaTime;
            if (_despawnCooldown <= _lifeTime / 3) Parpadear();
        }
    }

    void Parpadear()
    {
        if (_parpadeoCooldown <= 0)
        {
            if (visible) GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0); //Transparente
            else GetComponent<SpriteRenderer>().color = _originalColor;

            visible = !visible;
            _parpadeoCooldown = _timeBetweenParpadeo;
        }
        else _parpadeoCooldown -= Time.deltaTime;
    }

    void Spin()
    {
        if (t != null) t.Rotate(new Vector3(0, rotationSpeed * Time.deltaTime, 0));
    }
}
