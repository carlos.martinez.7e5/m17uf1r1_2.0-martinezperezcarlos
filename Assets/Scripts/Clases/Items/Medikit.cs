using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Medikit : Item
{
    [SerializeField] float healthRestored;
    public override void Interact(Yua yua)
    {
        yua.RestoreHealth(healthRestored);
    }
}
