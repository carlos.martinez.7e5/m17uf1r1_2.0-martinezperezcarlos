using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moneda : Item
{
    [SerializeField] int valorDinero;
    public override void Interact(Yua yua)
    {
        yua._pD.Dinero += valorDinero;
    }
}
