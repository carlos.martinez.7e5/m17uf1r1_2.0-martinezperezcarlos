using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBoost : Item
{
    [SerializeField] float speedBoost;
    [SerializeField] float powerUpDuration;

    public override void Interact(Yua yua)
    {
        yua.PowerUpSpeed(speedBoost, powerUpDuration);
    }
}
