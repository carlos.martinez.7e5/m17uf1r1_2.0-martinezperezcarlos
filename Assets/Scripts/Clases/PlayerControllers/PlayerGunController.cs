using System.Collections;
using UnityEngine;

public class PlayerGunController : MonoBehaviour
{
    public Player PlayerGunsData;
    public Arma armaEquipada;

    private SpriteRenderer sr;
    private GameObject player; //Usado para verificar la posicion respecto al arma, para hacer flip
    private Camera camara;

    private int armaIndex;
    private float shootCooldown;

    private float size;
    public bool canShoot;

    [SerializeField] AudioClip reloadClip;

    private void Start()
    {
        player = GameObject.Find("Player");
        camara = GameObject.Find("Main Camera").GetComponent<Camera>();

        sr = GetComponent<SpriteRenderer>();
        ArmaEquipada();

        armaIndex = 0;

        size = transform.lossyScale.x;
        canShoot = true;
    }

    private void Update()
    {
        ArmaEquipada(); //Mantiene actualizado el sprite del arma equipada
        CambiarArma(); //Comprueba si pulsa para cambiar de arma
        FixedGunRotation(); //GunRotation de Arma pero comprueba si esta de lado para hacer flip    
        Cooldown();
        Shoot(); //Comprueba si pulsa el boton y dispara
        Reload(); //Comprueba si pulsa el boton y recarga

    }

    void CambiarArma()
    {
        if (Input.GetKeyDown(KeyCode.X)) {

            if ((armaIndex + 1) >= PlayerGunsData.ArmasQueTengo.Length) armaIndex = 0;
            else armaIndex++;

            armaEquipada = PlayerGunsData.ArmasQueTengo[armaIndex];
            //Lo cambio tambien en el scriptable object para que se actualize bien el sprite
            PlayerGunsData.ArmaEquipada = PlayerGunsData.ArmasQueTengo[armaIndex];
            
            //Para evitar abusar, hago que al cambiar tambien tenga un poco de cooldown
            shootCooldown = armaEquipada.timeBetweenShoots / 2.5f;
        }
    }

    void Shoot()
    {
        if (canShoot)
        {
            if (Input.GetButtonDown("Fire1") && Cooldown())
            {
                if (armaEquipada.bullets >= 1)
                {
                    armaEquipada.Shoot();
                    GetComponent<AudioSource>().PlayOneShot(armaEquipada.shootSound);
                }
                else
                {
                    GetComponent<AudioSource>().PlayOneShot(reloadClip);
                    StartCoroutine(ReloadCorroutine());
                }
                shootCooldown = armaEquipada.timeBetweenShoots;
            }
        }  
    }

    void Reload()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            GetComponent<AudioSource>().PlayOneShot(reloadClip);
            StartCoroutine(ReloadCorroutine());
        }
    }

    IEnumerator ReloadCorroutine()
    {
        yield return new WaitForSeconds(armaEquipada.reloadTime);
        armaEquipada.Reload();
    }

    void ArmaEquipada()
    {
        armaEquipada = PlayerGunsData.ArmaEquipada;
        sr.sprite = armaEquipada.sprite;
    }

    void FixedGunRotation()
    {
        armaEquipada.GunRotation();

        if (camara == null) camara = GameObject.Find("Main Camera").GetComponent<Camera>();
        var mouse = camara.ScreenToWorldPoint(Input.mousePosition);
        mouse.z = 0;

        //Cambio todo el scale y no solo el sprite para que tambien se gire el arma con el personaje
        if (player.transform.position.x > mouse.x)
        {
            player.transform.localScale = new Vector3(-size, size, 1);
            sr.flipX = true;
            sr.flipY = true;
        }
        else { 
            player.transform.localScale = new Vector3(size, size, 1);
            sr.flipX = false;
            sr.flipY = false;
        } 
    }

    bool Cooldown()
    {
        if (shootCooldown >= 0)
        {
            shootCooldown -= Time.deltaTime;
            return false;
        }
        else return true;
    }
}
