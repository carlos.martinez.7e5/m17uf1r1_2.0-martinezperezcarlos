using System.Collections;
using UnityEngine;

public class Yua : MonoBehaviour, IDamageable
{
    public SpriteRenderer yuaSprite;
    private Rigidbody2D rb;

    public Player _pD;
    
    [SerializeField] AudioClip playerHit;
    [SerializeField] AudioClip shieldHit;
    public AudioClip pickUpItem;

    private void Start()
    {
        foreach (var arma in _pD.ArmasTotales)
        {
            arma.bullets = arma.startingCurrentBullets;
            arma.totalBullets = arma.startingTotalBullets - arma.startingCurrentBullets;
        }

        _pD.ArmasQueTengo = _pD.ArmasInicio;
        _pD.ArmaEquipada = _pD.ArmasQueTengo[0];

        _pD.speed = _pD.startingSpeed; //Para evitar que si muere sin haber terminado el tiempo del boost se mantenga para la nueva run

        yuaSprite = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();

        Debug.Log(Application.persistentDataPath);

        BaseEnemy.onEnemyKilled += AddMoney;
    }

    void AddMoney()
    {
        _pD.Dinero += 10;
    }

    private void FixedUpdate()
    {
        playerMovement();
    }

    private void playerMovement()
    {
        float mH = Input.GetAxis("Horizontal");
        float mV = Input.GetAxis("Vertical");

        if (mV == 0 && mH == 0) GetComponent<Animator>().SetBool("IsMoving", false);
        else GetComponent<Animator>().SetBool("IsMoving", true);

        Vector3 direction = new Vector3(mH * _pD.speed, mV * _pD.speed);
        rb.velocity = direction;
    }

    public void TakeDamage(float damageTaken)
    {
        //El escudo se tanquea el da�o, y lo absorbe todo.
        //P.e: Si tienes 5 de escudo y te hacen un golpe de 10, no se recibe da�o a la vida sino que para los
        //10 con todo el escudo, aunque sea menos.

        //Primero se golpea al escudo, si no tiene luego se golpea a la vida
        if (_pD.Escudo > 0)
        {
            GetComponent<AudioSource>().PlayOneShot(shieldHit);

            if ((_pD.Escudo -= damageTaken) >= 0) _pD.Escudo -= damageTaken;
            else _pD.Escudo = 0;            
        }
        else
        {
            GetComponent<AudioSource>().PlayOneShot(playerHit);
            
            _pD.health -= damageTaken;
            
            //Si la vida es menor que el 25% de la vida entra en panic mode
            if(_pD.health <= 25 * _pD.maxHealth / 100) GameObject.Find("AudioManager").GetComponent<AudioSource>().pitch = 1.25f;
            if (_pD.health <= 0) Die();
        }
    }
    
    private void Die()
    {
        /*Al hacer DontDestroyOnLoad en las escaleras aplica "para siempre", asi que ahora
         cuando se muere tengo que destruirlos manualmente*/
        Destroy(this.gameObject);
        Destroy(GameObject.Find("AudioManager"));
        
        SceneManagement.GoToScene("GameOverScene");
    }

    public void RestoreHealth(float healthRestored)
    {
        if (_pD.health + healthRestored <= _pD.maxHealth) _pD.health += healthRestored;
        else _pD.health = _pD.maxHealth;

        if (_pD.health > 25 * _pD.maxHealth / 100) GameObject.Find("AudioManager").GetComponent<AudioSource>().pitch = 1;   
    }

    public void RestoreShield(float shieldRestored)
    {
        if (_pD.Escudo + shieldRestored <= _pD.maxEscudo) _pD.Escudo += shieldRestored;
        else _pD.Escudo = _pD.maxEscudo;
    }

    public void PowerUpSpeed(float multiplier, float time)
    {
        //Le sube la velocidad por el multiplicador que le pase por parametro
        _pD.speed *= multiplier;

        //Comienza una corrutina para que el efecto dure X tiempo y luego se termine
        //Le pasa this (Yua) para que pueda acceder a los stats, el multiplicador para que se lo pueda restar
        //y el tiempo que dura

        StartCoroutine(RemovePowerUp(this,multiplier, time));
    }

    IEnumerator RemovePowerUp(Yua yua,float multiplier, float time)
    {
        yield return new WaitForSeconds(time);
        yua._pD.speed /= multiplier;
    }

    private void OnDisable()
    {
        BaseEnemy.onEnemyKilled -= AddMoney;
    }
}
