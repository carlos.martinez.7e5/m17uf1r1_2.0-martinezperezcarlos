using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class Config : MonoBehaviour
{
    // [SerializeField] Slider musicVolume, SfxVolume;
    // [SerializeField] Toggle muteMusic, muteSfx;

    [SerializeField] AudioMixer mixerMusic, mixerSfx;
    [SerializeField] Slider sliderMusic, sliderSfx;
    [SerializeField] Toggle muteMusic, muteSfx;

    private void Start()
    {
        sliderMusic.onValueChanged.AddListener(SetMusicVolume);
        sliderSfx.onValueChanged.AddListener(SetSfxVolume);

        sliderMusic.value = PlayerPrefs.GetFloat("MVolumen");
        sliderSfx.value = PlayerPrefs.GetFloat("SVolumen");
    }

    public void SetMusicVolume(float volume)
    {
        mixerMusic.SetFloat("MusicVolumen", volume);

        PlayerPrefs.SetFloat("MVolumen", volume);
    }

    public void MuteMusic(bool isMuted)
    {
        if (isMuted) {
            mixerMusic.SetFloat("MusicVolumen", -40);
        }
        else {
            mixerMusic.SetFloat("MusicVolumen", 0);
        }
    }

    public void SetSfxVolume(float volume)
    {
        mixerSfx.SetFloat("SfxMaster", volume);

        PlayerPrefs.SetFloat("SVolumen", volume);
    }

    public void MuteSfx(bool isMuted)
    {
        if (isMuted)
        {
            mixerSfx.SetFloat("SfxMaster", -40);
        }
        else { 
            mixerSfx.SetFloat("SfxMaster", 0);
        }
    }
}
