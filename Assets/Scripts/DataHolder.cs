using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataHolder : MonoBehaviour
{
    public delegate void DataH();

    public int killedEnemies, clearRooms, score;

    private void Start()
    {
        BaseEnemy.onEnemyKilled += EnemyKilled;
        BajarEscaleras.onRoomCompleted += RoomCompleted;
    }

    public void EnemyKilled()
    {
        killedEnemies++;
        score += 10;
    }

    public void RoomCompleted()
    {
        clearRooms++;
        score += 50;
    }

    private void OnDisable()
    {
        BaseEnemy.onEnemyKilled -= EnemyKilled;
        BajarEscaleras.onRoomCompleted -= RoomCompleted;
    }

    public void Reset()
    {
        killedEnemies = 0;
        clearRooms = 0;
        score = 0;
    }
}

[System.Serializable]
public class Data
{
    public int lastScore, maxScore, money;

    public Data(int last, int max, int calers)
    {
        lastScore = last;
        maxScore = max;
        money = calers;
    }
}
