using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BajarEscaleras : MonoBehaviour
{
    //public List<string> nombresEscenas;
    [SerializeField] LevelsList levels;

    public delegate void BEscalera();
    public static event BEscalera onRoomCompleted;

    /* La primera sala y la tienda no cuentan como salas superadas */
    [SerializeField] bool countAsARoom;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Yua player = collision.GetComponent<Yua>();

        if (player != null)
        {
            string nextSceneName;
            string currentSceneName = SceneManager.GetActiveScene().name;

            do
            {
                var randomIndex = Random.Range(0, levels.ListaDeEscenas.Count);
                nextSceneName = levels.ListaDeEscenas[randomIndex].name;

            } while (nextSceneName == currentSceneName);

            DontDestroyOnLoad(GameObject.Find("DontDestroy"));
            if (countAsARoom) onRoomCompleted();
            SceneManager.LoadScene(nextSceneName);
        }  
    }
}