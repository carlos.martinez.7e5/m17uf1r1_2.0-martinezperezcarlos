using UnityEngine;

public class Door : MonoBehaviour
{
    public DoorStates doorState;
    public Sprite openDoor, closedDoor;

    BoxCollider2D bc;
    SpriteRenderer sr;

    [SerializeField] AudioClip openClip;
    public Lever lever;

    public enum DoorStates
    {
        closed,
        open
    }

    private void Start()
    {
        bc = GetComponent<BoxCollider2D>();
        sr = GetComponent<SpriteRenderer>();

        doorState = DoorStates.closed;

        lever.OnActivateLever += OpenDoor;
    }

    void OpenDoor()
    {
        bc.enabled = false;
        sr.sprite = openDoor;
        GetComponent<AudioSource>().PlayOneShot(openClip);
    }

    private void OnDisable()
    {
        lever.OnActivateLever -= OpenDoor;
    }
}
