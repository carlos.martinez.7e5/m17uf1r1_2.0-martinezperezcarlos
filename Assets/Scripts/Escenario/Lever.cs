using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever : MonoBehaviour
{
    public LeverState leverState;
    [SerializeField] Sprite activatedS, desactivatedS;

    SpriteRenderer sr;

    public delegate void LeverD();
    public event LeverD OnActivateLever;

    /* StartEabled: La primera sala tiene que comenzar activada porque no hay enemigos y no puede aparecer */
    [SerializeField] bool startEnabled;

    public enum LeverState
    {
        activated,
        desactivated
    }

    void Start()
    {
        sr = GetComponent<SpriteRenderer>();

        leverState = LeverState.desactivated;

        OnActivateLever += SwitchState;
        EnemySpawner.OnAllEnemiesDied += enableLever;

        if (!startEnabled)
        {
            GetComponent<Collider2D>().enabled = false;
            GetComponent<SpriteRenderer>().enabled = false;
        }
    }

    void enableLever()
    {
        /* Lo iba a hacer con setEnable pero al estar desactivado se no contaba para el evento
         y daba null, asi que le quito el collider y el renderer para que "no este"*/
        
        GetComponent<Collider2D>().enabled = true;
        GetComponent<SpriteRenderer>().enabled = true;
    }

    void SwitchState()
    {         
        sr.sprite = activatedS;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Yua player = collision.GetComponent<Yua>();
        if (player != null) OnActivateLever();
    }

    private void OnDisable()
    {
        OnActivateLever -= SwitchState;
        EnemySpawner.OnAllEnemiesDied -= enableLever;
    }
}
