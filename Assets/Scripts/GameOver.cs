using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    DataHolder dataholder; //Los datos de esta partida
    Data jsonData; //Los datos que leera del Json
   
    [SerializeField] Text score, maxScore, enemies, rooms, newRecord;
    [SerializeField] Player player; //Referencia al scriptableObject para guardar el dinero
    
    void Start()
    {
        //Consigue los datos de esta partida
        dataholder = GameObject.Find("DataHolder").GetComponent<DataHolder>();

        //Establece la ruta donde leer / escribir el JSON
        string path = Application.persistentDataPath + "/scores.json";

        //Lee los datos guardados en el JSON (si existe), si no lo crea
        string jsonString;
        if (File.Exists(path)) 
        {
            jsonString = File.ReadAllText(path);
            //Convierte el string de JSON en un objeto tipo Data
            jsonData = JsonUtility.FromJson<Data>(jsonString);

            //El fichero puede existir pero estar en blanco
            if (jsonData == null) jsonData = new Data(0, 0,0);
        }
        else
        {
            File.Create(path);
            jsonData = new Data(0,0,0);
        }

        //Compara los datos de esta partida con los del fichero json
        
        if (dataholder.score > jsonData.maxScore) 
        {
            jsonData.maxScore = dataholder.score;
            newRecord.gameObject.SetActive(true);
        }

        //Sobreescribe el score de la �ltima partida y el dinero
        jsonData.lastScore = dataholder.score;
        jsonData.money = player.Dinero;

        //Convierte el resultado de vuelta a string, y lo sobreescribe de vuelta al json
        jsonString = JsonUtility.ToJson(jsonData);
        File.WriteAllText(path, jsonString);

        //Muestra por pantalla
        UIResults();
        
        //Destruyo en conjutno de cosas
        Destroy(GameObject.Find("DontDestroy"));
    }

    void UIResults()
    {
        score.text = dataholder.score.ToString();
        maxScore.text = jsonData.maxScore.ToString();
        enemies.text = dataholder.killedEnemies.ToString();
        rooms.text = dataholder.clearRooms.ToString();
    }
}
