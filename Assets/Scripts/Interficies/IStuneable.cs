public interface IStuneable
{
    void GetStunned(float timeStunned);
}
