using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] Text lastScore, maxScore;
    Data jsonData; //Los datos que leera del Json

    // Start is called before the first frame update
    void Start()
    {
        string path = Application.persistentDataPath + "/scores.json";

        //Lee los datos guardados en el JSON (si existe), si no lo crea
        string jsonString;
        if (File.Exists(path))
        {
            jsonString = File.ReadAllText(path);
            //Convierte el string de JSON en un objeto tipo Data
            jsonData = JsonUtility.FromJson<Data>(jsonString);

            //El fichero puede existir pero estar en blanco
            if (jsonData == null) jsonData = new Data(0, 0, 0);
        }
        else
        {
            File.Create(path);
            jsonData = new Data(0, 0, 0);
        }

        lastScore.text = jsonData.lastScore.ToString();
        maxScore.text = jsonData.maxScore.ToString();
    }
}
