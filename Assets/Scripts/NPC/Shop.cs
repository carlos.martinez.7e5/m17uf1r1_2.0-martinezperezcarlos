using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    //Para mostrar los objetos
    [SerializeField] ShopItems itemsSO;
    [SerializeField] GameObject item1, item2, item3;
    
    //Para cobrar al jugador
    [SerializeField] Player player; //El scriptableObject, que es lo que contiene sus datos (el dinero)
    [SerializeField] AudioClip caching;
    [SerializeField] AudioClip reload;

    //Para que le haga efecto lo que compre
    Yua yua;

    List<Item> itemsToDisplay = new List<Item>();

    private void Start()
    {
        yua = GameObject.Find("Player").GetComponent<Yua>();

        FillShopItems();
        UpdateItemsUI();
    }

    void FillShopItems()
    {
        itemsToDisplay = new List<Item>(); //Reinicia la lista para poder reutilizar la funci�n en el refill
        
        int randomItemIndex = 0;

        // De entre todos los items posibles, elige 3
        for (int i = 0; i < 3; i++)
        {
            do
            {
                randomItemIndex = Random.Range(0, itemsSO.itemList.Count);
                Debug.Log("Random Index = " + randomItemIndex);

                // Si la lista ya contiene el item que iba a escoger, busca otro para que no se repitan
            } while (itemsToDisplay.Contains(itemsSO.itemList[randomItemIndex]));

            //Lo a�ade a la lista
            itemsToDisplay.Add(itemsSO.itemList[randomItemIndex]);
        }    
    }

    void UpdateItemsUI()
    {
        GetImages();
        SetImages();
        SetPricesText();  
    }

    void GetImages()
    {
        item1.GetComponentInChildren<Image>().sprite = itemsToDisplay[0].GetComponent<Item>().sr.sprite;
        item2.GetComponentInChildren<Image>().sprite = itemsToDisplay[1].GetComponent<Item>().sr.sprite;
        item3.GetComponentInChildren<Image>().sprite = itemsToDisplay[2].GetComponent<Item>().sr.sprite;
    }

    void SetImages()
    {
        //Para que los botones se ajusten al tama�o de la imagen sin deformarse
        //Consigue el ratio de la imagen, dividiendo el ancho por el alto
        var ratio1 = itemsToDisplay[0].GetComponent<Item>().sr.sprite.rect.width /
            itemsToDisplay[0].GetComponent<Item>().sr.sprite.rect.height;

        //Consigue el bot�n
        var b1 = item1.GetComponentInChildren<Button>().gameObject;

        //Le cambia el localScale al bot�n para que se ajuste al ratio de la imagen
        b1.GetComponent<RectTransform>().localScale =
            new Vector3(b1.GetComponent<RectTransform>().localScale.x, b1.GetComponent<RectTransform>().localScale.x / ratio1, 1);
        //

        var ratio2 = itemsToDisplay[1].GetComponent<Item>().sr.sprite.rect.width /
            itemsToDisplay[1].GetComponent<Item>().sr.sprite.rect.height;

        var b2 = item2.GetComponentInChildren<Button>().gameObject;

        b2.GetComponent<RectTransform>().localScale =
            new Vector3(b2.GetComponent<RectTransform>().localScale.x, b2.GetComponent<RectTransform>().localScale.x / ratio2, 1);
        //

        var ratio3 = itemsToDisplay[2].GetComponent<Item>().sr.sprite.rect.width /
            itemsToDisplay[2].GetComponent<Item>().sr.sprite.rect.height;

        var b3 = item3.GetComponentInChildren<Button>().gameObject;

        b3.GetComponent<RectTransform>().localScale =
            new Vector3(b3.GetComponent<RectTransform>().localScale.x, b3.GetComponent<RectTransform>().localScale.x / ratio3, 1);
    }

    void SetPricesText()
    {
        //Actualiza el texto con el precio
        item1.GetComponentInChildren<Text>().text = itemsToDisplay[0].price.ToString();
        item2.GetComponentInChildren<Text>().text = itemsToDisplay[1].price.ToString();
        item3.GetComponentInChildren<Text>().text = itemsToDisplay[2].price.ToString();
    }

    public void BuyItem1()
    {
        if(player.Dinero >= itemsToDisplay[0].price)
        {
            GetComponent<AudioSource>().PlayOneShot(caching);
            player.Dinero -= itemsToDisplay[0].price;
            itemsToDisplay[0].Interact(yua);
        }
    }

    public void BuyItem2()
    {
        if (player.Dinero >= itemsToDisplay[1].price)
        {
            GetComponent<AudioSource>().PlayOneShot(caching);
            player.Dinero -= itemsToDisplay[1].price;
            itemsToDisplay[1].Interact(yua);
        }
    }

    public void BuyItem3()
    {
        if (player.Dinero >= itemsToDisplay[2].price)
        {
            GetComponent<AudioSource>().PlayOneShot(caching);
            player.Dinero -= itemsToDisplay[2].price;
            itemsToDisplay[2].Interact(yua);
        }
    }

    public void Refill()
    {
        if (player.Dinero >= 500)
        {
            GetComponent<AudioSource>().PlayOneShot(reload);

            player.Dinero -= 500;

            FillShopItems();
            UpdateItemsUI();
        }   
    }
}
