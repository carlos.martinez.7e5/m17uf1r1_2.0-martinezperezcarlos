using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tendero : MonoBehaviour
{
    [SerializeField] GameObject shopPanel;

    private void Start()
    {
        PauseMusic(true);
    }

    private void OnDisable()
    {
        PauseMusic(false);
    }

    void PauseMusic(bool pause)
    {
        var aS = GameObject.Find("AudioManager").GetComponent<AudioSource>();

        if (pause) aS.Pause();
        else aS.Play();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Yua player = collision.GetComponent<Yua>();

        if (player != null)
        {
            player.GetComponentInChildren<PlayerGunController>().canShoot = false;
            shopPanel.SetActive(true);
            Debug.Log("Bienvenido a mi tienda");
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Yua player = collision.GetComponent<Yua>();

        if (player != null)
        {
            player.GetComponentInChildren<PlayerGunController>().canShoot = true;
            shopPanel.SetActive(false);
            Debug.Log("Hasta mas ver");
        }
    }
}
