using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartLevelPosition : MonoBehaviour
{
    GameObject player;
    [SerializeField] float startX, startY;

    private void Start()
    {
        player = GameObject.Find("Player");
        player.transform.position = new Vector3(startX, startY, 0);
    }
}
