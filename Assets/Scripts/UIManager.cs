using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Player playerData;
    
    [Header("Vida")]
    public Slider slider;

    [Header("Municion")]
    public Text currentAmmo;
    public Text totalAmmo;

    [Header("Escudo")]
    public Slider shieldSlider;

    [Header("Dinero")]
    public Text money;

    private void Start()
    {
        slider.maxValue = playerData.maxHealth;
        shieldSlider.maxValue = playerData.maxEscudo;

        slider.value = playerData.health;
    }

    void Update()
    {
        UpdateHealt();
        UpdateAmmo();
        UpdateShield();
        UpdateMoney();
    }

    void UpdateAmmo()
    {
        currentAmmo.text = playerData.ArmaEquipada.bullets.ToString();
        totalAmmo.text = playerData.ArmaEquipada.totalBullets.ToString();
    }

    void UpdateHealt()
    {
        slider.value = playerData.health;
    }

    void UpdateShield()
    {
        shieldSlider.value = playerData.Escudo;
    }

    void UpdateMoney()
    {
        money.text = playerData.Dinero.ToString();
    }
}
