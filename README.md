# M17UF1R1_2.0-MartínezPérezCarlos

Autor: Carlos Martínez Pérez

CONTROLES: 
WASD - moverse
Click derecho - Disparar
R - Recargar
X - Cambiar de arma

REQUISITOS:
Uso de eventos:
	- Contador de enemigos matados
	- Contador de salas superadas
	- Hacer que la puerta se abra al activar la palanca

Uso de scriptable objects:
	- Datos de enemigos
	- Items / Consumibles
	- Armas
	- Datos del personaje
	- Lista de los niveles
	- Lista de los objetos que pueden aparecer en la tienda

Uso de corrutinas:
	- Recargar el arma
	- Stunear al enemigo con el taser
	- Power up de velocidad al utilizar el item
	- Gestionar la muerte de los enemigos

Power Ups:
	- Power up que te aumenta la velocidad durante unos segundos

Tienda:
	- Habitación con NPC, al acercarse a él aparece un panel en el que se puede hacer click para comprar armas / items.
	Cada vez la tienda será distinta, mostrando 3 de entre todos los objetos disponibles aleatoriamente. También se puede
	pagar para volver a rellenar la tienda con otros 3 items.

Añadidos al player:
	- Arma aturdidora / taser: Arma que dispara una bola eléctrica, no hace daño pero deja inmóvil al enemigo durante X segundos.

Musica y sonido:
	- 4 canciones de fondo distintas: Menú, Gameplay, Tienda, Game Over.
	Cuando al jugador le queda el 25% de la vida o menos el pitch aumenta, haciendo que la canción sea más aguda, vaya más rápido
	y dé tensión. Panic mode. Al restaurar salud la música vuelve a la normalidad.
	- Multiples sonidos: Diferentes sonidos de disparo según el arma, dañar al jugador, enemigo, abrir puertas, comprar items...

Progresión del juego:
	- 6 salas prehechas (5 de mazmorra, 1 de tienda). Cada una tiene dos escaleras, por la que bajaste y por la que debes bajar
	para continuar. Al bajar por las escaleras, el siguiente nivel se decide de manera aleatoria, por lo que hay muchas combinaciones
	para hacer runs únicas. 
	- Spawners aleatorios: Aunque la sala sea la misma, hay aleatoriedad en el numero de oleadas y numero de enemigos por oleadas.
	- Extra de puntuación por sala superada.
	- Utilización de eventos. Al eliminar a todos los enemigos aparece una palanca en el suelo. Al utilizar la palanca (pasar por encima)
	se utiliza un evento para abrir la puerta que no te debaja avanzar para el siguiente nivel.

UI:
	- Pantalla de inicio. Botón de jugar, salir y configuración. Se indica la última y máxima puntuación.
	- Pantalla de configuración de sonido. Permite modificar el sonido (musica y efectos independientemente) utilizando un slider.
	También mediante un toggle permite enmudecerlos.
	- Pantalla de game over. Indica puntuación de esta partida, máxima puntuación, monstruos elminiados y salas completadas. En caso
	de haber superado la máxima puntuación, se señala con un cartel. Esta pantalla también guarda los datos utilizando la serialización
	con JSON.